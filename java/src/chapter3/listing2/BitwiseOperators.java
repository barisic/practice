/**
 * BITWISE OPERATORS
 * << left shift
 * >> right shift
 * >>> right shift zero fill
 * & AND
 * | OR
 * ^ XOR
 * ~ NOT
 */

package chapter3.listing2;

public class BitwiseOperators {
    public static void main(String[] args) {
        System.out.println("LEFT SHIFT, RIGHT SHIFT AND RIGHT SHIFT ZERO FILL");
        System.out.println();
        System.out.println("FOR POSITIVE INTEGER");

        int a = 32;

        System.out.println(a + "->" + Integer.toBinaryString(a));
        System.out.println();

        int shiftedAToRight = a >> 4;
        int shiftedAToLeft = a << 4;
        int shiftedAToRightZeroFill = a >>> 4;

        System.out.println("Shifted to right: " + shiftedAToRight + "->" + Integer.toBinaryString(shiftedAToRight));
        System.out.println("Shifted to left: " + shiftedAToLeft + "->" + Integer.toBinaryString(shiftedAToLeft));
        System.out.println("Shifted to right zero fill: " + shiftedAToRightZeroFill + "->" + Integer.toBinaryString(shiftedAToRightZeroFill));
        System.out.println();

        System.out.println("FOR NEGATIVE INTEGER");

        int b = -32;

        System.out.println(b + "->" + Integer.toBinaryString(b));
        System.out.println();

        int shiftedBToRight = b >> 4;
        int shiftedBToLeft = b << 4;
        int shiftedBToRightZeroFill = b >>> 4;

        System.out.println("Shifted to right: " + shiftedBToRight + "->" + Integer.toBinaryString(shiftedBToRight));
        System.out.println("Shifted to left: " + shiftedBToLeft + "->" + Integer.toBinaryString(shiftedBToLeft));
        System.out.println("Shifted to right zero fill: " + shiftedBToRightZeroFill + "->" + Integer.toBinaryString(shiftedBToRightZeroFill));
        System.out.println();
        System.out.println();

        System.out.println("AND, OR, XOR, NOT");
        System.out.println();
        /**
         * Output for the code under this comment will consist of lines similar to:
         * AND - 2->(10), 3->(11), Result: 2->(10)
         * where the values in parenthesis are in binary form
         */
        int c = 2;
        int d = 3;

        System.out.println("AND - " + c + "->(" + Integer.toBinaryString(c) + "), " + d + "->(" + Integer.toBinaryString(d) + "), Result: " + (c & d) + "->(" + Integer.toBinaryString((c & d)) + ")");
        System.out.println("OR - " + c + "->(" + Integer.toBinaryString(c) + "), " + d + "->(" + Integer.toBinaryString(d) + "), Result: " + (c | d) + "->(" + Integer.toBinaryString((c | d)) + ")");
        System.out.println("XOR - " + c + "->(" + Integer.toBinaryString(c) + "), " + d + "->(" + Integer.toBinaryString(d) + "), Result: " + (c ^ d) + "->(" + Integer.toBinaryString((c & d)) + ")");
        System.out.println("NOT - " + c + "->(" + Integer.toBinaryString(c) + "), Result: " + (~c) + "->(" + Integer.toBinaryString(~c) + ")");
    }
}

