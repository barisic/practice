/**
 * This is example how to read values from command line
 */

package chapter3.listing2;

import java.util.*;

public class ReadFromCommandLine {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("What is your name? ");
        String name = in.nextLine();

        System.out.print("How old are you? ");
        int age = in.nextInt();

        System.out.println("Your name is " + name + " and you are " + age + " years old.");
    }
}


